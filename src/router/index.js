import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import SideNavigation from '../views/SideNavigation'
import BorrowMode from '../views/BorrowMode'
import Login from '../views/Login'
import Test from '../views/Test'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/side-navigation',
    name: 'Side Navigation',
    component: SideNavigation
  },
  {
    path: '/borrow-mode',
    name: 'Borrow Mode',
    component: BorrowMode
  },
  {
    path: '/login-page',
    name: 'Login',
    component: Login
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  }
]

const router = new VueRouter({
  routes
})

export default router
